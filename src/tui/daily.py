#!/usr/bin/env python3
# File Name: daily.py
# Description: Day view
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 21:12:35
# Last Modified: 25 Feb 2024 10:38:26

from datetime import date
from typing import Any, ClassVar

from rich.text import Text
from textual.binding import Binding, BindingType
from textual.message import Message
from textual.widgets import Label, ListItem, ListView

from remind import Reminder
from util import Maybe, add_minutes


def to_label(rem: Reminder) -> Label:
    t = Text(rem.body)
    if rem.red.is_just():
        t.stylize(style=f"rgb({rem.red.value},{rem.green.value},{rem.blue.value})")

    if rem.time.is_just():
        time = rem.time.value
        t = Text.assemble(f"({str(time.hour).rjust(2, '0')}:{str(time.minute).rjust(2, '0')}) ", t)

        if rem.duration.is_just():
            end = add_minutes(time, rem.duration.value)
            t.append(f" → ({str(end.hour).rjust(2, '0')}:{str(end.minute).rjust(2, '0')})")

    if rem.until.is_just():
        if rem.until.value == rem.date:
            t.append(" →|")
        else:
            t.append(" →")

    if rem.until.is_nothing() and rem.repeat.is_just():
        t.append(" ⟳")

    tags = ",".join([x for x in rem.tags])
    if len(tags) > 0:
        t2 = Text(" [" + tags + "]")
        t2.stylize(style="#7d7d7d")
        t.append(t2)

    return Label(t)


class DayWidget(ListView):
    BORDER_TITLE = "SELECTED DAY "

    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("j", "cursor_down", "Next", show=False),
        Binding("k", "cursor_up", "Prev", show=False),
        Binding("p", "postpone", "Postpone", show=True, key_display="p"),
    ]

    class Postpone(Message):
        def __init__(self, reminder: Reminder) -> None:
            self.reminder = reminder
            super().__init__()

    def __init__(self, reminds: list[Reminder], day: date, *args: Any, **kwargs: Any) -> None:
        super().__init__(*[ListItem(to_label(x)) for x in reminds], **kwargs)
        self.reminds: list[Reminder] = reminds

    def watch_has_focus(self, value: bool) -> None:
        if value:
            self.add_class("selected")
        else:
            self.remove_class("selected")

    def update(self, reminds: list[Reminder], day: date) -> None:
        self.reminds = reminds
        self.clear2()
        self.extend2(reminds)
        if len(self.reminds) > 0:
            self.index = 0

    def append2(self, reminder: Reminder) -> None:
        self.reminds.append(reminder)
        self.append(ListItem(to_label(reminder)))

    def extend2(self, reminds: list[Reminder]) -> None:
        self.reminds.extend(reminds)
        self.extend([ListItem(to_label(remind)) for remind in reminds])

    def clear2(self) -> None:
        self.reminds = []
        self.clear()

    def action_postpone(self) -> None:
        rem = self.get_selected()
        if rem.is_just():
            self.post_message(self.Postpone(rem.value))

    def get_selected(self) -> Maybe[Reminder]:
        i = self.index
        child: Maybe[Reminder] = Maybe.nothing()
        if i is not None and i >= 0 and i < len(self.reminds):
            child = Maybe.just(self.reminds[i])
        return child
