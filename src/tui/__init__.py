#!/usr/bin/env python3
# File Name: __init__.py
# Description: TUI stuff
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 17:24:44
# Last Modified: 14 Feb 2024 16:50:26

from .tui import CalendarApp
