#!/usr/bin/env python3
# File Name: tui.py
# Description: TUI
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Feb 2024 10:15:23
# Last Modified: 02 Mär 2024 12:08:40


from datetime import date, datetime, timedelta
from os import getenv, path
from typing import Any, ClassVar

from textual.app import App, ComposeResult
from textual.binding import Binding, BindingType
from textual.widgets import Footer, Header

from remind import Remind, Reminder, load_remind
from util import IO, Either, Maybe, append_to_file, parse_date, prev_year, remove_from_file, sys_call, todo_file, write_file

from .calendar import CalendarWidget
from .daily import DayWidget
from .screens import ErrorScreen, GotoScreen
from .todo import TodoWidget
from .upcoming import UpcomingWidget


def filter_upcoming(remind: Remind, today: date) -> list[Reminder]:
    return list(filter(
        lambda x: x.date >= today and
        x.date - timedelta(days=x.delta.default(0)) <= (today + timedelta(days=14)), remind)
    )


class CalendarApp(App):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("q", "quit", "Quit", show=False),
        Binding("right", "next_day", "Next Day", show=False),
        Binding("left", "prev_day", "Prev Day", show=False),
        Binding("l", "next_day", "Next Day", show=False),
        Binding("h", "prev_day", "Prev Day", show=False),
        Binding("L", "next_month", "Next Month", show=False),
        Binding("H", "prev_month", "Prev Month", show=False),
        Binding("shift+left", "prev_month", "Prev Month", show=False),
        Binding("shift+right", "next_month", "Next Month", show=False),
        Binding("t", "today", "Today", show=True, key_display="t"),
        Binding("e", "edit_reminder", "Edit", show=True, key_display="e"),
        Binding("a", "add_reminder", "Add", show=True, key_display="a"),
        Binding("g", "goto_date", "Goto", show=True, key_display="g"),
        Binding("T", "toggle_todo", "Todo", show=True, key_display="T"),
    ]
    CSS_PATH = "tui.tcss"

    def __init__(self, remind: Remind, filename: str) -> None:
        super().__init__()
        self.title = "wenn"
        self.filename = filename
        self.remind = remind
        self.todo_visible = False
        self.init_widgets()

    def init_widgets(self) -> None:
        today = datetime.now().date()
        reminds = list(filter(lambda x: x.date == today, [x for x in self.remind]))
        upcoming = filter_upcoming(self.remind, today)

        self.calendar_widget = CalendarWidget(self.remind, today)
        self.day_widget = DayWidget(reminds, today)
        self.upcoming_widget = UpcomingWidget(upcoming)
        self.todo_widget = TodoWidget(self.remind.todos)

    def compose(self) -> ComposeResult:
        yield Header()
        yield self.calendar_widget
        yield self.day_widget
        yield self.upcoming_widget
        yield Footer()

    def on_calendar_widget_changed(self, msg: CalendarWidget.Changed) -> None:
        self.day_widget.update(list(filter(lambda x: x.date == msg.date, [x for x in self.remind])), msg.date)

    def on_calendar_widget_extend(self, msg: CalendarWidget.Extend) -> None:
        self.reload(msg.date, self.calendar_widget.selected_date())

    def on_todo_widget_open(self, msg: TodoWidget.Open) -> None:
        todo = msg.todo

        def append() -> Either[None]:
            f = todo_file(self.filename)
            return append_to_file(f, ["REM 1 TAG todo MSG "]).run().bind(lambda _: self.open_in_editor(f, lineno=-1).run().bind(lambda _: final()))

        def final() -> Either[None]:
            d = self.calendar_widget.selected_date()
            self.reload(d, d)
            return Either.right(None)

        def action() -> Either[None]:
            if todo.is_just():
                return self.open_in_editor(todo.value.filename, lineno=todo.value.lineno).run().bind(lambda _: final())
            else:
                f = todo_file(self.filename)
                func = IO(append)
                if not path.isfile(f):
                    return write_file(f, []).run().bind(lambda _: func.run())
                else:
                    return func.run()

        self.handle_error(IO(action))

    def on_todo_widget_schedule(self, msg: TodoWidget.Schedule) -> None:
        d = self.calendar_widget.selected_date()

        def append() -> Either[None]:
            return append_to_file(self.filename, [f"REM {d.strftime('%Y-%m-%d')} MSG {msg.todo.body}"]).run()

        def final() -> Either[None]:
            self.reload(d, d)
            return Either.right(None)

        def action() -> Either[None]:
            pop_res = remove_from_file(msg.todo.filename, msg.todo.lineno - 1).run()
            return pop_res.bind(lambda _: append().bind(lambda _: final()))

        self.handle_error(IO(action))

    def on_day_widget_postpone(self, msg: DayWidget.Postpone) -> None:
        d = self.calendar_widget.selected_date()

        def append() -> Either[None]:
            f = todo_file(self.filename)
            func = append_to_file(f, [f"REM 1 TAG todo MSG {msg.reminder.body}"])
            if not path.isfile(f):
                return write_file(f, []).run().bind(lambda _: func.run())
            else:
                return func.run()

        def final() -> Either[None]:
            self.reload(d, d)
            return Either.right(None)

        def action() -> Either[None]:
            pop_res = remove_from_file(msg.reminder.filename, msg.reminder.lineno - 1).run()
            return pop_res.bind(lambda _: append().bind(lambda _: final()))

        self.handle_error(IO(action))

    def action_add_reminder(self) -> None:
        self.action_edit_reminder(add=True)

    def action_edit_reminder(self, add: bool = False) -> None:
        d = self.calendar_widget.selected_date()
        rem = Maybe.nothing() if add else self.day_widget.get_selected()

        def append() -> Either[None]:
            return append_to_file(self.filename, [f"REM {d.strftime('%Y-%m-%d')} MSG "]).run()

        def final() -> Either[None]:
            self.reload(d, d)
            return Either.right(None)

        def action() -> Either[None]:
            if rem.is_just():
                return self.open_in_editor(rem.value.filename, lineno=rem.value.lineno).run().bind(lambda _: final())
            else:
                return append().bind(lambda _: self.open_in_editor(self.filename, lineno=-1).run().bind(lambda _: final()))

        self.handle_error(IO(action))

    def reload(self, d: date, correct_date: date) -> None:
        # NOTE: Since it is reasonably fast it overwrites the current entries (which reduces complexity by alot).
        def update(rem: Remind) -> Either[None]:
            today = datetime.now().date()
            self.remind = rem
            self.calendar_widget.reload(rem, correct_date)
            self.upcoming_widget.update(filter_upcoming(self.remind, today))
            self.day_widget.update(
                list(filter(lambda x: x.date == d, [x for x in rem])), self.calendar_widget.selected_date()
            )
            self.todo_widget.update(rem.todos)
            return Either.right(None)

        def action() -> Either[None]:
            return load_remind(self.filename, d=prev_year(d), months=24).run().bind(lambda x: update(x))

        self.handle_error(IO(action))

    def open_in_editor(self, file: str, lineno: int = 0) -> IO[Either[str]]:
        def run_editor(editor: str) -> IO[Either[str]]:
            args = [editor]
            if editor == "nvim":
                if lineno > 0:
                    args.append(f"+{str(lineno)}")
                if lineno == -1:
                    args.append("+")
            args.append(file)
            return sys_call(" ".join(args), stdout=0)

        def with_suspend(call: IO[Either[str]]) -> Either[str]:
            self._driver.stop_application_mode()  # type: ignore
            x = call.run()
            self._driver.start_application_mode()  # type: ignore
            return x

        def action() -> Either[str]:
            return Maybe(getenv("EDITOR")).to_either(
                Exception("Could not find envvar 'EDITOR")
            ).bind(lambda x: with_suspend(run_editor(x)))

        return IO(action)

    def handle_error(self, f: IO[Either[Any]]) -> None:
        res = f.run()
        if res.is_left():
            self.push_screen(ErrorScreen(str(res.exception)))

    def action_goto_date(self) -> None:
        self.push_screen(GotoScreen(), self.change_date)

    def change_date(self, date_str: Maybe[str]) -> None:
        if date_str.is_just():
            res = parse_date(date_str.value).run()
            if res.is_right():
                self.calendar_widget.change_date(res.value)
            else:
                def action() -> Either[date]:
                    return res
                self.handle_error(IO(action))

    def action_quit(self) -> None:  # type: ignore
        self.exit()

    def action_toggle_todo(self) -> None:
        self.todo_visible = not self.todo_visible
        class_ = "todohidden"
        if self.todo_visible:
            self.todo_widget = TodoWidget(self.remind.todos)
            self.day_widget.remove_class(class_)
            self.mount(self.todo_widget, after=3)
        else:
            self.todo_widget.remove()
            self.day_widget.add_class(class_)

    def action_next_day(self) -> None:
        self.calendar_widget.action_next_day()

    def action_next_month(self) -> None:
        self.calendar_widget.action_next_month()

    def action_prev_day(self) -> None:
        self.calendar_widget.action_prev_day()

    def action_prev_month(self) -> None:
        self.calendar_widget.action_prev_month()

    def action_today(self) -> None:
        self.calendar_widget.action_today()
