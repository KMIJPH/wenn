#!/usr/bin/env python3
# # File Name: todo.py
# # Description: Todo list
# # Author: KMIJPH
# # Repository: codeberg.org/KMIJPH
# # License: GPL3
# # Creation Date: 10 Feb 2024 21:01:16
# # Last Modified: 25 Feb 2024 10:38:00

from typing import Any, ClassVar

from rich.text import Text
from textual.binding import Binding, BindingType
from textual.message import Message
from textual.widgets import Label, ListItem, ListView

from remind import Reminder
from util import Maybe


def to_label(todo: Reminder) -> Label:
    t = Text(todo.body)
    tags = ",".join([x for x in todo.tags if x != "todo"])

    if len(tags) > 0:
        t2 = Text(" [" + tags + "]")
        t2.stylize(style="#7d7d7d")
        t.append(t2)

    return Label(t)


class TodoWidget(ListView):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("a", "add", "Add", show=True, key_display="a"),
        Binding("e", "edit", "Edit", show=True, key_display="e"),
        Binding("s", "schedule", "Schedule", show=True, key_display="s"),
        Binding("j", "cursor_down", "Next", show=False),
        Binding("k", "cursor_up", "Prev", show=False),
    ]
    BORDER_TITLE = "TODO "

    class Open(Message):
        def __init__(self, todo: Maybe[Reminder]) -> None:
            self.todo = todo
            super().__init__()

    class Schedule(Message):
        def __init__(self, todo: Reminder) -> None:
            self.todo = todo
            super().__init__()

    def __init__(self, todos: list[Reminder], *args: Any, **kwargs: Any) -> None:
        super().__init__(*[ListItem(to_label(x)) for x in todos], **kwargs)
        self.todos = todos

    def watch_has_focus(self, value: bool) -> None:
        if value:
            self.add_class("selected")
        else:
            self.remove_class("selected")

    def update(self, todos: list[Reminder]) -> None:
        self.todos = todos
        self.clear2()
        self.extend2(todos)
        if len(self.todos) > 0:
            self.index = 0

    def action_add(self) -> None:
        self.post_message(self.Open(Maybe.nothing()))

    def append2(self, todo: Reminder) -> None:
        self.todos.append(todo)
        self.append(ListItem(to_label(todo)))

    def extend2(self, todos: list[Reminder]) -> None:
        self.todos.extend(todos)
        self.extend([ListItem(to_label(todo)) for todo in todos])

    def clear2(self) -> None:
        self.todos = []
        self.clear()

    def action_edit(self) -> None:
        i = self.index
        child: Maybe[Reminder] = Maybe.nothing()
        if i is not None and i >= 0 and i < len(self.todos):
            child = Maybe.just(self.todos[i])
        self.post_message(self.Open(child))

    def action_schedule(self) -> None:
        i = self.index
        if i is not None and i >= 0 and i < len(self.todos):
            child = self.todos[i]
            self.post_message(self.Schedule(child))
