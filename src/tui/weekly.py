#!/usr/bin/env python3
# File Name: weekly.py
# Description: Day view
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 21:12:35
# Last Modified: 14 Feb 2024 17:32:08

from datetime import date
from typing import Any

from textual.widgets import ListItem, ListView

from remind import Reminder

from .daily import to_label


class WeekWidget(ListView, can_focus=False):
    """
    View for a week.
    """

    BORDER_TITLE = "WEEK "

    def __init__(self, reminds: list[Reminder], day: date, *args: Any, **kwargs: Any) -> None:
        super().__init__(*[ListItem(to_label(x)) for x in reminds], **kwargs)
        self.reminds: list[Reminder] = reminds

    def update(self, reminds: list[Reminder], day: date) -> None:
        self.reminds = reminds
        self.clear2()
        self.extend2(reminds)
        if len(self.reminds) > 0:
            self.index = 0

    def append2(self, reminder: Reminder) -> None:
        self.reminds.append(reminder)
        self.append(ListItem(to_label(reminder)))

    def extend2(self, reminds: list[Reminder]) -> None:
        self.reminds.extend(reminds)
        self.extend([ListItem(to_label(remind)) for remind in reminds])

    def clear2(self) -> None:
        self.reminds = []
        self.clear()
