#!/usr/bin/env python3
# File Name: upcoming.py
# Description: Upcoming view
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 21:12:35
# Last Modified: 24 Feb 2024 23:10:19

from datetime import date, datetime
from typing import Any, Optional

from rich.text import Text
from textual.widgets import Label, ListItem, ListView

from remind import Reminder


def to_label(reminds: list[Reminder]) -> list[ListItem]:
    today = datetime.now().date()
    partitioned: dict[date, list[Reminder]] = {}
    for x in reminds:
        if x.date not in partitioned:
            partitioned[x.date] = []
        partitioned[x.date].append(x)

    labels = []
    for d, rems in partitioned.items():
        header = d.strftime("%b %d")
        if d == today:
            header = "Today"
        else:
            diff = (d - today).days
            if diff == 1:
                header = "Tomorrow"
            else:
                header = header + f" (in {diff} days)"
        labels.append(ListItem(Label(header, classes="dim")))

        for rem in rems:
            t = Text("  " + rem.body)
            if rem.red.is_just():
                t.stylize(style=f"rgb({rem.red.value},{rem.green.value},{rem.blue.value})")

            if rem.until.is_just():
                if rem.until.value == rem.date:
                    t.append(" →|")
                else:
                    t.append(" →")

            if rem.until.is_nothing() and rem.repeat.is_just():
                t.append(" ⟳")

            labels.append(ListItem(Label(t)))

    return labels


class UpcomingWidget(ListView, can_focus=False):
    BORDER_TITLE = " UPCOMING "

    def __init__(self, reminds: list[Reminder], *args: Any, **kwargs: Any) -> None:
        super().__init__(*to_label(reminds), initial_index=None, **kwargs)
        self.reminds: list[Reminder] = reminds

    def watch_index(self, old_index: Optional[int], new_index: Optional[int]) -> None:
        pass

    def update(self, reminds: list[Reminder]) -> None:
        self.reminds = reminds
        self.clear2()
        self.extend2(reminds)
        if len(self.reminds) > 0:
            self.index = 0

    def extend2(self, reminds: list[Reminder]) -> None:
        self.reminds.extend(reminds)
        self.extend(to_label(reminds))

    def clear2(self) -> None:
        self.reminds = []
        self.clear()
