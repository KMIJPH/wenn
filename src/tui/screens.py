#!/usr/bin/env python3
# File Name: screens.py
# Description:
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Feb 2024 14:37:48
# Last Modified: 24 Feb 2024 23:13:03

from typing import ClassVar

from textual.app import ComposeResult
from textual.binding import Binding, BindingType
from textual.containers import Container
from textual.screen import ModalScreen
from textual.widgets import Input, Label, TextArea

from util import Maybe


class ErrorScreen(ModalScreen):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("escape", "close", "Close", show=False),
    ]

    def __init__(self, error: str) -> None:
        self.error = error
        super().__init__()

    def compose(self) -> ComposeResult:
        with Container():
            yield TextArea(self.error, read_only=True, soft_wrap=True)
            yield Label("Press <escape> to close")

    def action_close(self) -> None:
        self.dismiss(True)


class GotoScreen(ModalScreen):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("escape", "close", "Close", show=False),
        Binding("enter", "submit", "Submit", show=False),
    ]

    def __init__(self) -> None:
        super().__init__()
        self.input = Input(restrict="[0-9,-]+")

    def compose(self) -> ComposeResult:
        with Container():
            yield Label("Enter date (Y-m-d)")
            yield self.input
            yield Label("Press <enter> to submit\nPress <escape> to close")

    def action_close(self) -> None:
        self.dismiss(Maybe.nothing())

    def on_input_submitted(self) -> None:
        value = self.input.value
        if value != "":
            self.dismiss(Maybe.just(value))

