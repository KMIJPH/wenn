#!/usr/bin/env python3
# File Name: calendar.py
# Description: Calendar view
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 20:58:58
# Last Modified: 25 Feb 2024 11:06:19


from datetime import date, timedelta
from typing import Any, ClassVar

from rich.text import Text
from textual.app import ComposeResult, RenderResult
from textual.binding import Binding, BindingType
from textual.events import Click
from textual.message import Message
from textual.widget import Widget
from textual.widgets import Label

from remind import Remind
from util import next_month, next_year, prev_month, prev_year


class CalendarDays(Widget, can_focus=False):
    """
    NOTE: Maybe use render_line.
    """

    def __init__(self, remind: Remind, today: date, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.remind: Remind = remind
        self.today: date = today
        self.selected_date: date = self.today

    def render(self) -> RenderResult:
        content: list[Any] = []
        current = date(self.selected_date.year, self.selected_date.month, 1)
        day_of_week = current.weekday()
        content.append("    " * day_of_week)

        while current.month == self.selected_date.month:
            sel = current.day == self.selected_date.day
            t = Text(f"{current.day:2d}")

            if current == self.today:
                t.stylize(style="bold")

            lookup = self.remind.reminders.get(current)
            if lookup is not None and len(lookup) > 0:
                colored = list(filter(lambda x: x.red.is_just(), lookup))
                if len(colored) > 0:
                    c = colored[0]
                    t.stylize(style=f"underline rgb({c.red.value},{c.green.value},{c.blue.value})")
                else:
                    t.stylize(style="underline")

            if sel:
                t = Text.assemble(Text("[", style="bold"), t)
                t.append(Text("]", style="bold"))
            else:
                t = Text.assemble(" ", t)
                t.append(" ")

            content.append(t)

            if current.weekday() == 6:  # sunday
                content.append("\n")

            current += timedelta(days=1)
        return Text.assemble(*content)


class CalendarWidget(Widget, can_focus=True):
    BINDINGS: ClassVar[list[BindingType]] = [
        Binding("j", "next_week", "Next Week", show=False),
        Binding("k", "prev_week", "Prev Week", show=False),
        Binding("down", "next_week", "Next Week", show=False),
        Binding("up", "prev_week", "Prev Week", show=False),
    ]

    class Changed(Message):
        def __init__(self, date: date) -> None:
            self.date = date
            super().__init__()

    class Extend(Message):
        def __init__(self, date: date) -> None:
            self.date = date
            super().__init__()

    def __init__(self, remind: Remind, today: date, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.width: int = 27
        self.height: int = 7
        self.today: date = today
        self.header: date = today
        self.range = (prev_year(today), next_year(today))

        self.label = Label(today.strftime("%B %Y"), classes="title")
        self.calendar = CalendarDays(remind, today)

    def compose(self) -> ComposeResult:
        yield self.label
        yield Label(" Mo  Tu  We  Th  Fr  Sa  Su")
        yield self.calendar

    def changed(self) -> None:
        self.post_message(self.Changed(self.calendar.selected_date))

    def on_click(self, event: Click) -> None:
        height = self.height  - 2  # adjust for header row
        width = self.width
        x = event.x
        y = event.y
        if (y <= height and y >= 0) and (x <= width and x >= 0):
            clicked_column = event.x // 4  # each day cell is 4 characters wide
            clicked_row = y
            days_offset = self.calendar.selected_date.replace(day=1).weekday()
            clicked_day = clicked_column + clicked_row * 7 - days_offset
            self.change_date(self.calendar.selected_date.replace(day=1) + timedelta(days=clicked_day))

    def watch_has_focus(self, value: bool) -> None:
        if value:
            self.add_class("selected")
        else:
            self.remove_class("selected")

    def on_mouse_scroll_down(self) -> None:
        self.action_next_week()

    def on_mouse_scroll_up(self) -> None:
        self.action_prev_week()

    def reload(self, remind: Remind, d: date) -> None:
        self.range = (prev_year(d), next_year(d))
        self.calendar.remind = remind
        self.label.update(d.strftime("%B %Y"))
        self.header = d
        self.change_date(d)

    def change_date(self, d: date) -> None:
        if d < next_month(self.range[0]):
            self.post_message(self.Extend(d))
            self.range = (prev_year(d), next_year(d))

        if d > prev_month(self.range[1]):
            self.post_message(self.Extend(d))
            self.range = (prev_year(d), next_year(d))

        if self.header.month != d.month or self.header.year != d.year:
            self.header = d
            self.label.update(d.strftime("%B %Y"))

        self.calendar.selected_date = d
        self.changed()
        self.calendar.refresh()

    def selected_date(self) -> date:
        return self.calendar.selected_date

    def action_next_day(self) -> None:
        self.change_date(self.calendar.selected_date + timedelta(days=1))

    def action_prev_day(self) -> None:
        self.change_date(self.calendar.selected_date - timedelta(days=1))

    def action_next_week(self) -> None:
        self.change_date(self.calendar.selected_date + timedelta(days=7))

    def action_prev_week(self) -> None:
        self.change_date(self.calendar.selected_date - timedelta(days=7))

    def action_next_month(self) -> None:
        self.change_date(next_month(self.calendar.selected_date))

    def action_prev_month(self) -> None:
        self.change_date(prev_month(self.calendar.selected_date))

    def action_today(self) -> None:
        self.change_date(self.today)
