#!/usr/bin/env python3
# File Name: writer.py
# Description: Remind to ics
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 12 Feb 2024 12:13:30
# Last Modified: 29 Aug 2024 18:47:12

import uuid
from datetime import datetime, timedelta

from util import tz_offset

from .type import Reminder


def rem_to_event(rem: Reminder, tzo: float) -> str:
    """
    Converts a reminder to a iCalendar Event.
    https://datatracker.ietf.org/doc/html/rfc5545
    ..
    https://datatracker.ietf.org/doc/html/rfc7986
    """
    dstr = rem.date.strftime("%Y%m%d")

    # always present
    lines = [
        "BEGIN:VEVENT",
        f"UID:wenn.{uuid.uuid4().hex}",
        f"SUMMARY:{rem.body}",
        f"DTSTAMP:{datetime.now().strftime('%Y%m%dT%H%M%SZ')}",
    ]

    if rem.time.is_just():
        combined = datetime.combine(rem.date, rem.time.value) - timedelta(hours=tzo)
        lines.append(f"DTSTART:{combined.strftime('%Y%m%dT%H%M%SZ')}")
        if rem.duration.is_just():
            add = combined + timedelta(minutes=rem.duration.value)
            lines.append(f"DTEND:{add.strftime('%Y%m%dT%H%M%SZ')}")
    else:
        lines.append(f"DTSTART;VALUE=DATE:{dstr}")

    lines.append("END:VEVENT")

    return "\r\n".join(lines)


def to_ics(reminders: list[Reminder]) -> str:
    """
    Converts a list of reminders to the iCalendar format.
    """
    tzo = tz_offset()
    return "\r\n".join(["BEGIN:VCALENDAR", "VERSION:2.0", "PRODID:https://codeberg.org/KMIJPH/wenn", "\r\n".join([rem_to_event(x, tzo) for x in reminders]), "END:VCALENDAR"])
