#!/usr/bin/env python3
# File Name: reader.py
# Description: Parse remind output
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 16:05:40
# Last Modified: 18 Feb 2024 08:49:24

import json
from datetime import date
from typing import Any, Optional

from util import IO, Either, sys_call

from .type import Remind


def from_json(output: str) -> Either[list[dict[str, Any]]]:
    """
    https://man.archlinux.org/man/remind.1
    """
    return Either(json.loads(output))


def load_remind(filename: str, months: int = 24, d: Optional[date] = None) -> IO[Either[Remind]]:
    """
    Runs remind and load objects.
    """
    set_date = " " + d.strftime("%Y-%m-%d") if d is not None else ""
    out_res = sys_call(f"remind -ppp{months} {filename}{set_date}").run()

    def action(output: str) -> Either[Remind]:
        return from_json(output).bind(lambda j: Either(Remind(j)))

    return IO(lambda: out_res.bind(action))
