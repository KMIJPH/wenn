#!/usr/bin/env python3
# File Name: type.py
# Description: Remind types
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 16:07:32
# Last Modified: 02 Mär 2024 11:58:09

import calendar
from datetime import date, time
from typing import Any, Iterator

from util import Maybe, parse_date, parse_time

month_names = list(calendar.month_name)
month_names_lower = [name.lower() for name in month_names]


class Reminder:
    """
    https://man.archlinux.org/man/rem2ps.1.en#REM2PS_PARTIAL_JSON_INPUT_FORMAT_(-PP_OPTION)
    """

    def __init__(self, **kwargs: Any):
        # always present
        self.date: date = parse_date(kwargs["date"]).run().value
        self.filename: str = kwargs["filename"]
        self.lineno: int = kwargs["lineno"]
        self.body: str = kwargs.get("rawbody") if "rawbody" in kwargs else kwargs["body"]
        calendar_body = kwargs.get("calendar_body") if "calendar_body" in kwargs else kwargs.get("plain_body")
        if calendar_body:
            self.body = calendar_body

        # meta
        self.tags: list[str] = []
        tags = kwargs.get("tags")
        if tags:
            self.tags = tags.split(",")
        self.repeat: Maybe[int] = Maybe(kwargs.get("rep"))
        self.delta: Maybe[int] = Maybe(kwargs.get("delta"))
        self.red: Maybe[int] = Maybe(kwargs.get("r"))
        self.blue: Maybe[int] = Maybe(kwargs.get("b"))
        self.green: Maybe[int] = Maybe(kwargs.get("g"))
        self.until: Maybe[date] = Maybe(kwargs.get("until")).bind(lambda x: parse_date(x).run().to_maybe())

        # time
        self.time: Maybe[time] = Maybe(kwargs.get("eventstart")).bind(lambda x: parse_time(x).run().to_maybe())
        self.duration: Maybe[int] = Maybe(kwargs.get("eventduration"))

    def __lt__(self, other: 'Reminder') -> bool:
        return self.date < other.date

    def __gt__(self, other: 'Reminder') -> bool:
        return self.date > other.date

    def __key(self) -> tuple[str, date, Maybe[time], str, int]:
        return (self.body, self.date, self.time, self.filename, self.lineno)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Reminder):
            return NotImplemented

        return self.__key() == other.__key()

    def __str__(self) -> str:
        return str(vars(self))

    def is_todo(self) -> bool:
        return "todo" in self.tags


class Remind:
    """
    https://man.archlinux.org/man/rem2ps.1.en#REM2PS_PURE_JSON_INPUT_FORMAT_(-PPP_OPTION)
    """

    def __init__(self, input: list[dict[str, Any]]):
        self.reminders: dict[date, list[Reminder]] = {}
        self.todos: list[Reminder] = []
        for m in input:
            entries_raw = m["entries"]
            entries = [Reminder(**e) for e in entries_raw]
            todos = [item for item in entries if item.is_todo()]
            reminds = [item for item in entries if not item.is_todo()]

            for reminder in reminds:
                if reminder.date not in self.reminders:
                    self.reminders[reminder.date] = []
                self.reminders[reminder.date].append(reminder)

            self.update_todos(todos)

    def __str__(self) -> str:
        return str(vars(self))

    def __iter__(self) -> Iterator[Reminder]:
        for _, reminders in self.reminders.items():
            yield from reminders

    def __add__(self, other: 'Remind') -> 'Remind':
        for reminder in other:
            if reminder.date in self.reminders:
                self.reminders[reminder.date] = []

            if reminder not in self.reminders[reminder.date]:
                self.reminders[reminder.date].append(reminder)

        self.update_todos(other.todos)

        return self

    def update_todos(self, todos: list[Reminder]) -> None:
        # The same todo could be defined with a different date. Check body only.
        for todo in todos:
            if not any(x.body == todo.body for x in self.todos):
                self.todos.append(todo)
