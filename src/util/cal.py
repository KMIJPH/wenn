#!/usr/bin/env python3
# File Name: cal.py
# Description: Calendar/date stuff
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Feb 2024 11:59:58
# Last Modified: 11 Nov 2024 18:48:32

import time as t
from datetime import date, datetime, time, timedelta

from .monads import IO, Either, either


def tz_offset() -> float:
    current_time = t.time()
    local_time = t.localtime(current_time)
    offset_seconds = local_time.tm_gmtoff
    return offset_seconds / 3600


def parse_date(date_str: str) -> IO[Either[date]]:
    @either
    def action() -> date:
        return datetime.strptime(date_str, "%Y-%m-%d").date()

    return IO(action)


def parse_time(date_str: str) -> IO[Either[time]]:
    @either
    def action() -> time:
        return datetime.strptime(date_str, "%Y-%m-%dT%H:%M").time()

    return IO(action)


def add_minutes(t: time, minutes: int) -> time:
    d = datetime(2010, 1, 1, t.hour, t.minute, t.second)
    return (d + timedelta(minutes=minutes)).time()


def prev_month(d: date) -> date:
    first_of_month = d.replace(day=1)
    last_of_prev_month = first_of_month - timedelta(days=1)
    clamped_day = min(d.day, last_of_prev_month.day)
    return last_of_prev_month.replace(day=clamped_day)


def next_month(d: date) -> date:
    first_of_next_month = d.replace(day=1) + timedelta(days=32)
    first_of_next_month = first_of_next_month.replace(day=1)
    if first_of_next_month.month == 12:
        num_days_next_month = (
            first_of_next_month.replace(year=first_of_next_month.year + 1, month=1)
            - first_of_next_month
        ).days
    else:
        num_days_next_month = (
            first_of_next_month.replace(month=first_of_next_month.month + 1)
            - first_of_next_month
        ).days

    clamped_day = min(d.day, num_days_next_month)
    return first_of_next_month.replace(day=clamped_day)


def prev_year(d: date) -> date:
    try:
        return d.replace(year=d.year - 1)
    except Exception:
        return d.replace(year=d.year - 1, day=28)


def next_year(d: date) -> date:
    if d.month == 2 and d.day == 29:
        return d.replace(year=d.year + 1, day=28)
    else:
        return d.replace(year=d.year + 1)


def diff_month(d1: date, d2: date) -> int:
    return (d1.year - d2.year) * 12 + d1.month - d2.month
