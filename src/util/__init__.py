#!/usr/bin/env python3
# File Name: __init__.py
# Description: Utility stuff
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 16:04:38
# Last Modified: 29 Aug 2024 17:13:53

from .cal import add_minutes, diff_month, next_month, next_year, parse_date, parse_time, prev_month, prev_year, tz_offset
from .monads import IO, Either, Maybe, either
from .system import append_to_file, die_with_error, read_file, remove_from_file, sys_call, todo_file, write_file
