#!/usr/bin/env python3
# File Name: monads.py
# Description: Monad types
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 16:04:11
# Last Modified: 18 Feb 2024 15:43:17


from functools import wraps
from typing import Any, Callable, Generic, Optional, TypeVar, Union

A = TypeVar('A')
B = TypeVar('B')


class IO(Generic[A]):
    def __init__(self, action: Callable[[], A]):
        self._action = action

    def __str__(self) -> str:
        return 'IO'

    def __repr__(self) -> str:
        return 'IO()'

    def bind(self, func: Callable[[A], 'IO[A]']) -> 'IO[A]':
        return IO(lambda: func(self._action()).run())

    def run(self) -> A:
        return self._action()

    @staticmethod
    def pure(value: A) -> 'IO[A]':
        return IO(lambda: value)


class Maybe(Generic[A]):
    def __init__(self, value: Optional[A]):
        self._value: Optional[A] = value

    def __str__(self) -> str:
        return f'Just {self._value}' if self._value is not None else 'Nothing'

    def __repr__(self) -> str:
        return f'Maybe({self._value})'

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Maybe):
            return self._value == other._value
        return False

    def bind(self, func: Callable[[A], 'Maybe[B]']) -> 'Maybe[B]':
        if self._value is not None:
            return func(self._value)
        else:
            return Maybe(None)

    def default(self, default: A) -> A:
        return self._value if self._value is not None else default

    def map(self, func: Callable[[A], B]) -> 'Maybe[B]':
        if self._value is not None:
            return Maybe(func(self._value))
        else:
            return Maybe(None)

    def to_either(self, e: Exception) -> 'Either[A]':
        if self.is_just():
            return Either.right(self.value)
        else:
            return Either.left(e)

    def is_just(self) -> bool:
        return self._value is not None

    def is_nothing(self) -> bool:
        return self._value is None

    @property
    def value(self) -> A:
        return self._value  # type: ignore

    @staticmethod
    def just(value: A) -> 'Maybe[A]':
        return Maybe(value)

    @staticmethod
    def nothing() -> 'Maybe[A]':
        return Maybe(None)


class Either(Generic[A]):
    def __init__(self, value: Union[A, Exception]):
        self._value = value

    def __str__(self) -> str:
        return f'Right {self._value}' if not self.is_left() else f'Left {self._value}'

    def __repr__(self) -> str:
        return f'Either({self._value})'

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Either):
            return self._value == other._value
        return False

    def bind(self, func: Callable[[A], 'Either[B]']) -> 'Either[B]':
        if isinstance(self._value, Exception):
            return Either(self._value)
        else:
            try:
                return func(self._value)
            except Exception as e:
                return Either(e)

    def map(self, func: Callable[[A], B]) -> 'Either[B]':
        if isinstance(self._value, Exception):
            return Either(self._value)
        else:
            try:
                return Either(func(self._value))
            except Exception as e:
                return Either(e)

    def default(self, default: A) -> A:
        return self._value if not isinstance(self._value, Exception) else default

    def to_maybe(self) -> Maybe[A]:
        if self.is_right():
            return Maybe.just(self.value)
        else:
            return Maybe.nothing()

    def is_right(self) -> bool:
        return not isinstance(self._value, Exception)

    def is_left(self) -> bool:
        return isinstance(self._value, Exception)

    @property
    def value(self) -> A:
        return self._value  # type: ignore

    @property
    def exception(self) -> Exception:
        return self._value  # type: ignore

    @staticmethod
    def right(value: A) -> 'Either[A]':
        return Either(value)

    @staticmethod
    def left(error: Exception) -> 'Either':
        return Either(error)


def either(func: Callable[..., A]) -> Callable[..., Either[A]]:
    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Either[A]:
        try:
            result = func(*args, **kwargs)
            return Either.right(result)
        except Exception as e:
            return Either.left(e)
    return wrapper
