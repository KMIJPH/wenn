#!/usr/bin/env python3
# File Name: system.py
# Description: System stuff
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 12 Feb 2024 09:40:56
# Last Modified: 18 Feb 2024 08:47:50

import subprocess
import sys
from os import path

from .monads import IO, Either, either


def die_with_error(e: Exception) -> None:
    sys.stderr.write(f"{e.__class__.__name__}: {str(e)}\n")
    sys.exit(1)


def sys_call(command: str, shell: bool = True, check: bool = True, stdout: int = subprocess.PIPE, stderr: int = subprocess.PIPE) -> IO[Either[str]]:
    def action() -> Either[str]:
        try:
            result = subprocess.run(command, shell=shell, check=check, stdout=stdout, stderr=stderr)
            if stdout != 0:
                output = result.stdout.decode().strip()
                return Either.right(output)
            else:
                return Either.right("")
        except subprocess.CalledProcessError as e:
            error_output = e.stderr.decode().strip()
            return Either.left(Exception(error_output))
        except Exception as e:
            return Either.left(e)

    return IO(action)


def write_file(p: str, lines: list[str]) -> IO[Either[None]]:
    @either
    def action() -> None:
        with open(p, "w") as file:
            file.writelines(lines)

    return IO(action)


def read_file(p: str) -> IO[Either[list[str]]]:
    @either
    def action() -> list[str]:
        with open(p, "r") as file:
            return file.readlines()

    return IO(action)


def append_to_file(p: str, lines: list[str]) -> IO[Either[None]]:
    def append(l1: list[str], l2: list[str]) -> list[str]:
        l1.extend(l2)
        return l1

    def action() -> Either[None]:
        return read_file(p).run().bind(lambda x: write_file(p, append(x, lines)).run())

    return IO(action)


def remove_from_file(p: str, i: int) -> IO[Either[None]]:
    @either
    def remove(lines: list[str]) -> list[str]:
        lines.pop(i)
        return lines

    def action() -> Either[None]:
        return read_file(p).run().bind(lambda lines: remove(lines).bind(lambda x: write_file(p, x).run()))

    return IO(action)


def todo_file(p: str) -> str:
    """
    Returns the directory to the todo file,
    which is always relative to the main file provided.
    """
    dir = path.dirname(p)
    return path.join(dir, "todo.rem")
