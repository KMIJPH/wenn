#!/usr/bin/env python3
# File Name: main.py
# Description: Remind TUI
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 10 Feb 2024 14:41:10
# Last Modified: 11 Nov 2024 18:49:06

import argparse
from datetime import datetime

from remind import load_remind, to_ics
from tui import CalendarApp
from util import die_with_error, parse_date, prev_year

# TODO: allow custom css path?
# TODO: ...?


def main() -> None:
    parser = argparse.ArgumentParser(
        description="Calendar TUI using remind", prog="wenn"
    )
    subparsers = parser.add_subparsers(title="command", dest="command")
    parser.add_argument(
        "-v", "--version", action="version", version="%(prog)s 0.1.2", default=False
    )

    tui_parser = subparsers.add_parser("tui", help="Run TUI")
    tui_parser.add_argument("filename", help="The .rem filename to process")

    export_parser = subparsers.add_parser("export", help="Export calendar to .ics")
    export_parser.add_argument("filename", help="The .rem filename to process")
    export_parser.add_argument(
        "-m",
        "--months",
        help="Number of months in advance to calculate the calendar",
        type=int,
        default=12,
    )
    export_parser.add_argument(
        "-d", "--date", help="Starting date for the calendar (Y-m-d)", type=str
    )

    args = parser.parse_args()

    if args.command == "tui":
        today = datetime.now().date()
        rem_res = load_remind(args.filename, months=24, d=prev_year(today)).run()
        if rem_res.is_right():
            app = CalendarApp(rem_res.value, args.filename)
            app.run()
        else:
            die_with_error(rem_res.exception)
    elif args.command == "export":
        d = None
        if args.date is not None:
            date_res = parse_date(args.date).run()
            if date_res.is_left():
                die_with_error(date_res.exception)
            else:
                d = date_res.value

        rem_res = load_remind(args.filename, months=args.months, d=d).run()
        if rem_res.is_right():
            print(to_ics([x for x in rem_res.value]))
        else:
            die_with_error(rem_res.exception)
    else:
        help = parser.format_help()
        die_with_error(Exception("No command specified\n" + help))


if __name__ == "__main__":
    main()
