#!/usr/bin/env python3
# File Name: test_ics.py
# Description: Rem2ics tests
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 16 Feb 2024 15:17:52
# Last Modified: 25 Feb 2024 11:34:40


import sys
import unittest

sys.path.append("src")  # fmt: off

from remind import Reminder, to_ics


class Tests(unittest.TestCase):
    def test_to_ics(self) -> None:
        rem1 = Reminder(date="2022-01-02", filename="main.rem", lineno=1, rawbody="ASDF")
        rem1_str = "\r\n".join(
            [
                "BEGIN:VCALENDAR",
                "VERSION:2.0",
                "PRODID:https://codeberg.org/KMIJPH/wenn",
                "BEGIN:VEVENT",
                "SUMMARY:ASDF",
                "DTSTART;VALUE=DATE:20220102",
                "END:VEVENT",
                "END:VCALENDAR"
            ]
        )
        res1 = [x for x in to_ics([rem1]).split("\r\n") if not x.startswith("DTSTAMP") and not x.startswith("UID")]
        self.assertEqual("\r\n".join(res1), rem1_str)

        rem2 = Reminder(date="2024-01-02", eventstart="2024-01-02T14:00", filename="main.rem", lineno=1, rawbody="Reminder")
        rem2_str = "\r\n".join(
            [
                "BEGIN:VCALENDAR",
                "VERSION:2.0",
                "PRODID:https://codeberg.org/KMIJPH/wenn",
                "BEGIN:VEVENT",
                "SUMMARY:ASDF",
                "DTSTART;VALUE=DATE:20220102",
                "END:VEVENT",
                "BEGIN:VEVENT",
                "SUMMARY:Reminder",
                "DTSTART:20240102T140000Z",
                "END:VEVENT",
                "END:VCALENDAR"
            ]
        )
        res2 = [x for x in to_ics([rem1, rem2]).split("\r\n") if not x.startswith("DTSTAMP") and not x.startswith("UID")]
        self.assertEqual("\r\n".join(res2), rem2_str)


if __name__ == "__main__":
    unittest.main()

