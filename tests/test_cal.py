# File Name: test_cal.py
# Description: Calendar tests
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Feb 2024 12:14:16
# Last Modified: 25 Feb 2024 11:35:17

import sys
import unittest
from datetime import date

sys.path.append("src")  # fmt: off

from util import diff_month, next_month, next_year, prev_month, prev_year


class Tests(unittest.TestCase):
    def test_diff(self) -> None:
        self.assertEqual(diff_month(date(2024, 2, 1), date(2022, 2, 1)), 24)
        self.assertEqual(diff_month(date(2022, 12, 31), date(2023, 2, 1)), -2)
        self.assertEqual(diff_month(date(2023, 12, 31), date(2023, 2, 1)), 10)

    def test_month(self) -> None:
        self.assertEqual(next_month(date(2024, 2, 1)), date(2024, 3, 1))
        self.assertEqual(next_month(date(2024, 2, 29)), date(2024, 3, 29))
        self.assertEqual(next_month(date(2024, 1, 30)), date(2024, 2, 29))
        self.assertEqual(prev_month(date(2024, 3, 30)), date(2024, 2, 29))
        self.assertEqual(prev_month(date(2022, 1, 1)), date(2021, 12, 1))

    def test_year(self) -> None:
        self.assertEqual(next_year(date(2024, 2, 1)), date(2025, 2, 1))
        self.assertEqual(next_year(date(2024, 2, 29)), date(2025, 2, 28))
        self.assertEqual(prev_year(date(2024, 3, 30)), date(2023, 3, 30))
        self.assertEqual(prev_year(date(2024, 2, 29)), date(2023, 2, 28))


if __name__ == "__main__":
    unittest.main()

