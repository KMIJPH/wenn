# wenn

A simple TUI calendar front-end to [remind](https://dianne.skoll.ca/projects/remind/).

There's already a lot of tools around remind (see [remind helpers](https://dianne.skoll.ca/projects/remind/)),
but none of them fit my needs.

![](./etc/screenshot.png)

## Usage

Run `wenn <command> --help` for additional info.
```
usage: wenn [-h] {tui,export} ...

Calendar TUI using remind

options:
  -h, --help    show this help message and exit

command:
  {tui,export}
    tui         Run TUI
    export      Export calendar to .ics
```

## Reminders

In order to add or edit reminders, a little knowledge about the [remind scripting language](https://man.archlinux.org/man/remind.1#REMINDER_FILES) is required.
Reminder colours are adopted from their colour specified in their remind files:

```
REM 2024-03-28 THROUGH 2024-04-02 SPECIAL COLOR 30 140 30 Holidays
```

## TODO

There is no native TODO functionality in remind.
`wenn` uses a separate file (`todo.rem` in the same directory as `<filename>`) for todos which are identified by their `todo` TAG:

```remind
# todo.rem
# Reminding on the first of a month (`REM 1`) is just an example, as all reminders need a date.
# The date information for todos is omitted by wenn.
REM 1 TAG todo TAG important MSG Important thing
REM 1 TAG todo MSG Do chores
```

which can be included in the main .rem file:
```remind
# main.rem
include path_to_todo.rem
...
```

## Keybinds

TBD
